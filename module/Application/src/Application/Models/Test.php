<?php
namespace Application\Models;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;

class Test
{
    function __construct($nocache = 0)
    {
        $this->cacheTime = 3600;
        $this->noCache = $nocache;
    }
################################################################################ 
    function question1_calc()
    {
        $data = array();
        $key_txt = md5('Question_1');
        $cache = $this->maMemCache($this->cacheTime, $key_txt);
        $data = $cache->getItem($key_txt, $success);
		if( empty($data) || ($this->noCache == 1) )
		{
            $value = 3;
            for($i = 0; $i < 5; $i++)
            {
                $value = $value + (2 * $i);
            }

            $data = $value;

            $cache->setItem($key_txt, $data);
        }
        return($data);
    }

    function question2_calc()
    {
        $data = array();
        $key_txt = md5('Question_2');
        $cache = $this->maMemCache($this->cacheTime, $key_txt);
        $data = $cache->getItem($key_txt, $success);
        if( empty($data) || ($this->noCache == 1) )
        {

            $result = 99;
            $value = (($result - (10 * 2)) - 24);
            $data = $value;

            $cache->setItem($key_txt, $data);
        }
        return($data);
    }

    function question3_calc()
    {
        $data = array();
        $key_txt = md5('Question_3');
        $cache = $this->maMemCache($this->cacheTime, $key_txt);
        $data = $cache->getItem($key_txt, $success);
        if( empty($data) || ($this->noCache == 1) )
        {
            $value = 5;
            for($i = 2; $i <= 5; $i++)
            {
                $value = $i.$value;
            }

            $data = $value;

            $cache->setItem($key_txt, $data);
        }
        return($data);
    }
################################################################
    function maMemCache($time, $namespace)
    {
        $cache = StorageFactory::factory([
											    'adapter' => [
											        'name' => 'filesystem',
											        'options' => [
											            'namespace' => $namespace,
											            'ttl' => $time,
											        ],
											    ],
											    'plugins' => [
											        // Don't throw exceptions on cache errors
											        'exception_handler' => [
											            'throw_exceptions' => true
											        ],
											        'Serializer',
											    ],
											]);
		return($cache);
	}
################################################################################ 
}
