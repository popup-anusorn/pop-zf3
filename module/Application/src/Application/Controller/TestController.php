<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Test;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;

class TestController extends AbstractActionController
{
    public function __construct()
    {
        $this->cacheTime = 36000;
    }

    public function basic()
    {
        $view = new ViewModel();
        $view->action = $this->params()->fromRoute('action', 'index');
        return $view;       
    } 

    public function indexAction() 
    {
        try
        {
            $view = $this->basic();
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }

    public function question1Action() 
    {
        try
        {
            $view = $this->basic();

            $models = new Test();
            $view->x = $models->question1_calc();

            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }

    public function question2Action() 
    {
        try
        {
            $view = $this->basic();

            $models = new Test();
            $view->y = $models->question2_calc();

            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }

    public function question3Action() 
    {
        try
        {
            $view = $this->basic();

            $models = new Test();
            $view->x = $models->question3_calc();

            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
}